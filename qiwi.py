#!/usr/bin/env python3

"""
   Qiwi
    a quick GUI for QEMU
    by Ethan Gates
    https://gitlab.com/eg-tech/qiwi
"""

import argparse
import os
import sys
import subprocess
import json
import shlex
import platform
from dyngooey import Gooey, GooeyParser, gooey_stdout, gooey_id
from colored import stylize, attr, fg

# Set location and file name for config file
args_file = os.path.expanduser("~/qiwi-config.json")

# Needed for properly bundling app with PyInstaller
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)

# Necessary function for "dyngooey" functionality
def dumps(data):
    return json.dumps(data, indent=4, default=str)

# General Gooey GUI info (must come before "main" function)
@Gooey(
    program_name='Qiwi',
 #   program_description='Quick interface for QEMU',
    image_dir=resource_path("images"),
    default_size=(850,600),
    header_bg_color="#999999",
    body_bg_color="#999999",
    footer_bg_color="#999999",
    sidebar_bg_color="#999999",
    terminal_panel_color="#999999",
    richtext_controls=True,
    tabbed_groups=True,
    clear_before_run = True,
    menu=[{
        'name': 'Help',
        'items': [{
            'type': 'AboutDialog',
            'menuTitle': 'About',
            'name': 'Qiwi',
            'description': 'Quick interface for QEMU',
            'version': '0.6.0',
            'copyright': '2023',
            'developer': 'Ethan Gates',
            'website': 'https://gitlab.com/EG-tech/qiwi'
            },
            {
            'type': 'Link',
            'menuTitle': 'QEMU QED',
            'url': 'https://eaasi.gitlab.io/program_docs/qemu-qed'
        }]
    }]
)

def main():

    parser = GooeyParser()
    subs = parser.add_subparsers(dest="qiwi_task")
    emulate_parser = subs.add_parser('emulate')
    img_parser = subs.add_parser('make_new_image')
    convert_parser = subs.add_parser('convert_image')

    #Set fields + groups for GUI selection
    # create "groups" to bundle related settings
    machine_group = emulate_parser.add_argument_group(
        "Machine Settings",
        "Select emulated hardware",
        gooey_options={
            'columns': 2
        }
    )
    storage_group = emulate_parser.add_argument_group(
        "Storage Settings",
        "Select disk images to mount"
    )
    feature_group = emulate_parser.add_argument_group(
        "Other",
        "Enable/disable other features"
    )
    img_group = img_parser.add_argument_group(
        "New System Image",
        "Use QEMU's disk image utility to make a new disk image to use as a system drive"
    )
    convert_group = convert_parser.add_argument_group(
        "Convert Disk Images",
        "Use QEMU's disk image utility to convert existing disk images"
    )

    # add individual fields for emulate subparser
    machine_group.add_argument(
        "-cpu_architecture",
        required=True,
        metavar="System Emulator",
        help="Specify what type of system you would like to emulate",
        choices=['32-bit PC', '64-bit PC', 'PowerPC']
    )
    machine_group.add_argument(
        "-memory_size",
        required=True,
        metavar="Memory",
        help="Specify how much RAM (in MB)",
        type=int,
    )
    machine_group.add_argument(
        "-video_hardware",
        required=False,
        metavar="Graphics Card",
        help="Select an emulated graphics card",
        choices=['standard VGA', 'Cirrus CLGD 54xx', 'Spice QXL', 'Virtio', 'VMWare SVGA'],
    )
    machine_group.add_argument(
        "-sound_hardware",
        required=False,
        metavar="Sound Card",
        help="Select an emulated sound card",
        choices=["Creative Sound Blaster 16", "Intel AC97", "Yamaha YM3812", "ENSONIQ ES1370", "Gravis Ultrasound GF1", "Intel HD Audio"],
    )
    machine_group.add_argument(
        "-network_hardware",
        required=False,
        metavar="Network Adapter",
        help="Select an emulated network device",
        choices=['AMD PCNET', 'RealTek RTL8139', 'Novell NE2000 PCI', 'Novell NE2000 ISA', 'Intel Gigabit', 'SunGEM'],
    )
    machine_group.add_argument(
        "-attach_tablet",
        required=False,
        metavar="Attach USB tablet",
        help="Check to replace default mouse with an emulated USB tablet",
        action='store_true',
    )

    storage_group.add_argument(
        "-drive_image_path",
        required=True,
        metavar="System Disk Image",
        help="Select your virtual system disk/drive",
        widget="FileChooser",
        gooey_options=dict(wildcard="QCOW image (*.qcow, *.qcow2, *.img)|*.qcow;*.qcow2;*.img"),
    )
    storage_group.add_argument(
        "-drive_snapshot_mode",
        required=False,
        metavar="Snapshot mode",
        help="Run with your system drive 'read only' (any changes will be discarded after emulation)",
        action='store_true',
    )
    storage_group.add_argument(
        "-cdrom_image_path",
        required=False,
        metavar="CD-ROM Disk Image",
        help="Select an ISO",
        widget="FileChooser",
        gooey_options=dict(wildcard="CDROM image (*.iso, *.img, *.cdr)|*.iso;*.img;*.dmg;*.cdr"),
    )
    storage_group.add_argument(
        "-floppy_image_path",
        required=False,
        metavar="Floppy Disk Image",
        help="Select a floppy disk image",
        widget="FileChooser",
        gooey_options=dict(wildcard="Floppy image (*.img, *.fda, *.fdd)|*.img;*.fda;*.fdd"),
    )
    storage_group.add_argument(
        "-shared_folder",
        required=False,
        metavar="Shared Folder",
        help="Attach shared folder as Virtual FAT storage",
        widget="DirChooser",
    )

    feature_group.add_argument(
        "-enable_hypervisor",
        metavar="Enable virtualization",
        help="Select a compatible hypervisor accelerator to speed up your guest session",
        choices=['KVM (Linux)', 'Hypervisor Framework (macOS)', 'Windows Hypervisor Platform (Windows)', 'Xen (Linux)']
    )
    feature_group.add_argument(
        "-enable_internet",
        metavar="Enable internet access",
        help="Check to enable user mode network capable of connecting to internet",
        action='store_true',
    )
    feature_group.add_argument(
        "-add_flag",
        metavar="(Advanced) Add QEMU flag",
        help="Arbitrarily add flags to your QEMU command. Not recommended unless you know what you're doing!",
        action='store',
    )

    # add individual fields for img subparser

    img_group.add_argument(
        "-format",
        required=True,
        metavar="Format",
        help="Select a disk image format for your new system drive",
        choices=['Raw', 'Virtual']
    )
    img_group.add_argument(
        "-size",
        required=True,
        metavar="Size (in GB)",
        help="Specify the size of your new disk image (in GB)",
        type=int,
    )
    img_group.add_argument(
        "-name",
        required=True,
        metavar="Filename",
        help="Name your new disk image and save location",
        widget="FileSaver",
        gooey_options={
            'wildcard':
                "New Disk Image|*.img",
        }
    )

    # add invididual fields for convert subparser

    convert_group.add_argument(
        "-source_image",
        required=True,
        metavar="Image to Convert",
        help="Select an existing disk image to convert or compress",
        widget="FileChooser",
    )
    convert_group.add_argument(
        "-source_format",
        required=False,
        metavar="Format of Image to Convert",
        help="Specify the format of the source image to convert or compress",
        choices=['Raw', 'QCOW2 (QEMU)', 'VMDK (VMWare)', 'VDI (VirtualBox)', 'VHD (Virtual PC)', 'VHDX (Hyper-V)'],
    )
    convert_group.add_argument(
        "-output_image",
        required=True,
        metavar="Output Image",
        help="Name your new disk image and save location",
        widget="FileSaver",
    )
    convert_group.add_argument(
        "-output_format",
        required=True,
        metavar="Format of New Image",
        help="Specify the format of your new disk image",
        choices=['Raw', 'QCOW2', 'VMDK (VMWare)', 'VDI (VirtualBox)', 'VHD (Virtual PC)', 'VHDX (Hyper-V)'],
    )
    convert_group.add_argument(
        "-optional_compression",
        required=False,
        metavar="Losslessly Compress New Image?",
        help="(QCOW2 output only) If enabled, compress new image using zlib",
        action='store_true',
    )

# Set initial values (if present in qiwi-config.json) and any further user selections or adjustments and make those values "dynamic" (can be manually cleared/reset back to null on subsequent runs within the Qiwi session)
# With dyngooey.py, code adapted from GitHub user "pasbec": https://github.com/chriskiehl/Gooey/issues/751#issuecomment-942413467

    # detect if qiwi-config.json is present and load selections if so
    stored_args = {}

    if os.path.isfile(args_file):
        with open(args_file) as data_file:
            stored_args = json.load(data_file)

    if gooey_stdout():

        dynamic_values = {
            'cpu_architecture': stored_args.get('cpu_architecture'),
            'memory_size': stored_args.get('memory_size'),
            'video_hardware': stored_args.get('video_hardware'),
            'sound_hardware': stored_args.get('sound_hardware'),
            'network_hardware': stored_args.get('network_hardware'),
            'attach_tablet': stored_args.get('attach_tablet'),
            'drive_image_path': stored_args.get('drive_image_path'),
            'drive_snapshot_mode': stored_args.get('drive_snapshot_mode'),
            'cdrom_image_path': stored_args.get('cdrom_image_path'),
            'floppy_image_path': stored_args.get('floppy_image_path'),
            'shared_folder': stored_args.get('shared_folder'),
            'enable_hypervisor': stored_args.get('enable_hypervisor'),
            'enable_internet': stored_args.get('enable_internet'),
            'add_flag': stored_args.get('add_flag')
        }

        seeds = {}
        for action in emulate_parser._actions:
            action_seeds = seeds.setdefault(gooey_id(action), {})
            if action.dest in dynamic_values:
                action_seeds["value"] = dynamic_values[action.dest]

        print(dumps(seeds), file=gooey_stdout())

    args = parser.parse_args()

    # build QEMU command for emulate subparser
    # first need to check if on macOS and account for the various locations where QEMU could be installed; otherwise .app bundles will not work correctly
    if platform.system() == 'Darwin':
        # MacPorts
        if os.path.isfile("/opt/local/bin/qemu-system-i386"):
            mac_path = "/opt/local/bin/"
        # Homebrew on Apple Silicon
        elif os.path.isfile("/opt/homebrew/bin/qemu-system-i386"):
            mac_path = "/opt/homebrew/bin/"
        # Homebrew on Apple Intel
        else:
            mac_path = "/usr/local/bin/"

    if args.qiwi_task == "emulate":

        if platform.system() == 'Darwin':
            cmd = mac_path
        else:
            cmd = ""

        if args.cpu_architecture == "32-bit PC":
            cmd += "qemu-system-i386"
        elif args.cpu_architecture == "64-bit PC":
            cmd += "qemu-system-x86_64"
        elif args.cpu_architecture == "PowerPC":
            cmd += "qemu-system-ppc"

        cmd += " -drive file='%s'" % (args.drive_image_path)
        # run qemu-img detection to get and specify disk image format for system drive
        if platform.system() == 'Darwin':
            qemu_img = mac_path + "qemu-img"
        else:
            qemu_img = "qemu-img"
        image_info = subprocess.run([qemu_img, "info", "--output=json", args.drive_image_path], stdout=subprocess.PIPE)
        image_info = json.loads(image_info.stdout)
        cmd += ",format=%s" % (image_info.get('format'))

        if args.drive_snapshot_mode:
            cmd += " -snapshot"

        cmd += " -m %d" % (args.memory_size)

        if args.video_hardware is None:
            print("Default graphics card")
        elif args.video_hardware == "standard VGA":
            cmd += " -vga std"
        elif args.video_hardware == "Cirrus CLGD 54xx":
            cmd += " -vga cirrus"
        elif args.video_hardware == "VMWare SVGA":
            cmd += " -vga vmware"
        elif args.video_hardware == "Spice QXL":
            cmd += " -vga qxl"
        elif args.video_hardware == "Virtio":
            cmd += " -vga virtio"

        if args.sound_hardware is None:
            print("Default sound card")
        elif args.sound_hardware == "Creative Sound Blaster 16":
            cmd += " -device sb16"
        elif args.sound_hardware == "Intel AC97":
            cmd += " -device ac97"
        elif args.sound_hardware == "Yamaha YM3812":
            cmd += " -device adlib"
        elif args.sound_hardware == "ENSONIQ ES1370":
            cmd += " -device es1370"
        elif args.sound_hardware == "Gravis Ultrasound GF1":
            cmd += " -device gus"
        elif args.sound_hardware == "Intel HD Audio":
            cmd += " -device intel-hda"

        if args.network_hardware is None:
            print("No network adapter")
        elif args.network_hardware == "AMD PCNET":
            cmd += " -net nic,model=pcnet"
        elif args.network_hardware == "RealTek RTL8139":
            cmd += " -net nic,model=rtl8139"
        elif args.network_hardware == "Novell NE2000 PCI":
            cmd += " -net nic,model=ne2k_pci"
        elif args.network_hardware == "Novell NE2000 ISA":
            cmd += " -net nic,model=ne2k_isa"
        elif args.network_hardware == "Intel Gigabit":
            cmd += " -net nic,model=e1000"
        elif args.network_hardware == "SunGEM":
            cmd += " -net nic,model=sungem"

        if args.attach_tablet:
            cmd += " -usbdevice tablet"

        if args.enable_internet:
            cmd += " -net user"

        if args.enable_hypervisor is None:
            print("Virtualization disabled")
        elif args.enable_hypervisor == "KVM (Linux)":
            cmd += " -accel kvm"
        elif args.enable_hypervisor == "Hypervisor Framework (macOS)":
            cmd += " -accel hvf"
        elif args.enable_hypervisor == "Windows Hypervisor Platform (Windows)":
            cmd += " -accel whpx"
        elif args.enable_hypervisor == "Xen (Linux)":
            cmd += " -accel xen"

        if args.cdrom_image_path is None:
            print("No CD-ROM")
        else:
            cmd += " -cdrom '%s'" % (args.cdrom_image_path)
        if args.floppy_image_path is None:
            print("No Floppy Disk")
        else:
            cmd += " -fda '%s'" % (args.floppy_image_path)
        if args.shared_folder is None:
            print("No shared folder")
        else:
            cmd += " -drive file=fat:rw:'%s',format=vvfat,media=disk" % (args.shared_folder)
        if args.add_flag is None:
            cmd += ""
        else:
            cmd += " %s" % (args.add_flag)

        # put constructed command line into gooey log window for debugging
        print(cmd)
        # tell system to execute QEMU command; shell=False is better approach for security, but then subprocess.run requires a list of  arguments rather than a single string, so use shlex.split to break cmd string into pieces (this also helps make sure file path expansion works on both *nix and Windows shells)
        subprocess.run(shlex.split(cmd),shell=False)

        # save latest user selections to qiwi-config.json file
        with open(args_file, 'w') as data_file:
            json.dump(vars(args), data_file)

    # build qemu-img command for img subparser
    elif args.qiwi_task == "make_new_image":

        if platform.system() == 'Darwin':
            cmd = mac_path + "qemu-img create"
        else:
            cmd = "qemu-img create"

        if args.format == "Raw":
            cmd += " -f raw"
        elif args.format == "Virtual":
            cmd += " -f qcow2"

        cmd += " '%s'" % (args.name)
        cmd += " %dG" % (args.size)

        # put constructed command line into gooey log window for debugging
        print(cmd)
        # tell system to execute qemu-img command
        subprocess.run(shlex.split(cmd),shell=False)

    # build qemu-img command for convert subparser
    elif args.qiwi_task == "convert_image":

        if platform.system() == 'Darwin':
            cmd = mac_path + "qemu-img convert"
        else:
            cmd = "qemu-img convert"
        
        cmd += " '%s'" % (args.source_image)
        
        if args.source_format == "Raw":
            cmd += " -f raw"
        elif args.source_format == "QCOW2 (QEMU)":
            cmd += " -f qcow2"
        elif args.source_format == "VMDK (VMWare)":
            cmd += " -f vmdk"
        elif args.source_format == "VDI (VirtualBox)":
            cmd += " -f vdi"
        elif args.source_format == "VHD (Virtual PC)":
            cmd += " -f vhd"
        elif args.source_format == "VHDX (Hyper-V)":
            cmd += " -f vhdx"

        if args.output_format == "Raw":
            cmd += " -O raw"
        elif args.output_format == "QCOW2":
            cmd += " -O qcow2"
        elif args.output_format == "VMDK (VMWare)":
            cmd += " -O vmdk"
        elif args.output_format == "VDI (VirtualBox)":
            cmd += " -O vdi"
        elif args.output_format == "VHD (Virtual PC)":
            cmd += " -O vhd"
        elif args.output_format == "VHDX (Hyper-V)":
            cmd += " -O vhdx"

        if args.optional_compression:
            cmd += " -c"

        cmd += " '%s'" % (args.output_image)

        # put constructed command into gooey log window for debugging
        print(cmd)
        # tell system to execute qemu-img convert command
        subprocess.run(shlex.split(cmd),shell=False)

if __name__ == '__main__':
    main()
