# Qiwi
A quick, emulation-focused, cross-platform launcher for QEMU, written in Python using [Gooey](https://github.com/chriskiehl/Gooey)

All documentation for Qiwi has moved to the GitLab repository's [Wiki](https://gitlab.com/EG-tech/qiwi/-/wikis/home)! Please consult the wiki for:

- [Project background and statement of purpose](https://gitlab.com/EG-tech/qiwi/-/wikis/Intro)
- How to get [up and running with Qiwi](https://gitlab.com/EG-tech/qiwi/-/wikis/Running%20Qiwi) on your OS or using the qiwi.py script
- Guidance on [using Qiwi](https://gitlab.com/EG-tech/qiwi/-/wikis/Usage) to configure/launch QEMU sessions
- [Guidelines for contributing to Qiwi](https://gitlab.com/EG-tech/qiwi/-/wikis/Contributing)